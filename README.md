# Library Assistant is a considerable system that send you notifications regarding the deadline of borrowed books

## A simple automatically DevOps pipline providing notifications to avoid you missing the deadline to return your borrowed books.

- Web scraping by webdriver to
    - login library website
    - parse information regarding return deadline, books' titles and so on
- Store information in Dropbox to tracking what I read so far 
- Setup CICD pipelin and run it periodically to receive the deadline notifications
- Send out email to inform the status of reading plans and return deadlines

## How to install this discord bot project

1. Clone this project
2. Create virtual environment and install requirements.txt - 
   `virtualenv --no-site-packages --distribute .env &&\
    source .env/bin/activate &&\
    pip install -r requirements.txt`
3. Setup your connection and properties on library
4. Create a account on Dropbox and setup the connection as well 
5. Execute project `python lib_assist.py` or make a CICD pipeline in gitlib

## How to tweak this project for your own uses

I'd encourage you to clone and rename this project to use for your own puposes. It's a good starter to build up your CICD pipeline

## Known issues (Work in progress)

Because I am not the member of this library in Munich anymore, I did not update the CICD process. And hopefully, there won't be a requirement if I become a member of a modern library somewhere elsa.

## Like this project?

If you are feeling generous, please click star button and feel free to drop some messages.