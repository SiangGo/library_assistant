from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
import smtplib
from decouple import config

EMAIL_PASSWORD = config('EMAIL_PASSWORD', default='')
username = config('lib_username', default='')
password = config('lib_password', default='')
auth_code = config('dropbox_auth_code', default='')


def send_mail(message, subject='ETF reporting', receiver='jyw5415@gmail.com', files=[]):
    EMAIL_ADDRESS = 'jyw5415@gmail.com'

    msg = MIMEMultipart()
    msg['From'] = EMAIL_ADDRESS
    msg['To'] = receiver
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    bcc = "jyw5415@gmail.com"
    rcpt = bcc.split(",") + [receiver]

    nl = '\n'
    html = f"<p style= {chr(34)}font-family: arial; font-size:11pt; font-style:regular{chr(34)}>{message.replace(nl, '<br>')}</p>"
    msg.attach(MIMEText(html, "html"))

    for f in files:
        part = MIMEApplication(
            'application',
            'octet-stream'
        )
        with open(f, "rb") as fil:
            part.set_payload(fil.read())

        part.add_header('Content-Disposition',
                        'attachment; filename="{}"'.format(f.split('/')[-1]))
        msg.attach(part)

    with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        smtp.sendmail(EMAIL_ADDRESS, rcpt, msg.as_string().encode('utf-8'))

        print(f'send email to myself successfully')