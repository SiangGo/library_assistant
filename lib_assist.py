from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
from time import sleep
from collections import defaultdict
from func import *
import datetime


if __name__ == "__main__":
    url = "https://ssl.muenchen.de/aDISWeb/app?service=direct/0/Home/$DirectLink&sp=SOPAC&sp=SBK00000000"

    options = webdriver.ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--window-size=1420,1080')
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')

    # driver = webdriver.Chrome("./chromedriver", options=options)  # for local use
    driver = webdriver.Chrome(options=options)  # for docker use

    driver.get(url)
    #
    driver.find_element_by_id("IDENT_1").send_keys(username)
    driver.find_element_by_id("LPASSW_1").send_keys(password)
    driver.find_element_by_css_selector("input[value=\"Anmeldung abschicken\"]").click()

    #
    print("login successfully")
    sleep(3)
    
    try:
        driver.find_element_by_xpath("//*[@data-fld=\"RGLINK_5\"]").click()

        html = driver.page_source

        """web scraping"""
        soup = BeautifulSoup(html, 'html.parser')

        books_info_1st = soup.find_all('td', attrs={'class': ''})
        books_info = soup.find_all('td', attrs={'class': 'rTable_td_text'})
        books_info = books_info_1st + books_info
        books_info = [el.text.replace('\n', '') for el in books_info]

        d = defaultdict(list)
        str_len = len(books_info)
        cols_name = ['deadline', 'place', 'book', 'extension']
        for c, col in enumerate(cols_name):
            d[col] = [books_info[i] for i in range(c, str_len, 4)]

        df = pd.DataFrame.from_dict(d)
        print('////////////////////////')

        df = df[['deadline', 'place', 'book']]
        df.to_csv("lib_assist_current.csv", index=False, encoding='utf-8')
        print(df.to_string(max_colwidth=30))
        # close web browser
        driver.close()

        """ connect to dropbox to ingest the log files"""
        import dropbox
        filename = "lib_assist.csv"

        with dropbox.Dropbox(oauth2_access_token=auth_code) as dbx:
            dbx.users_get_current_account()
            print("Successfully set up client!")

            # download files
            metadata, res = dbx.files_download(path=f"/{filename}")

            with open(filename, 'wb') as file:
                file.write(res.content)

            """ diff prev lib info and current lib info"""
            df_prev = pd.read_csv(filename)

            # extract new info
            df_new = pd.concat([df_prev, df]).drop_duplicates(keep=False)

            # distinguish return or borrow
            prev_return = df_new.merge(df_prev, how='inner', on=list(df_new.columns))
            prev_return['return or borrow'] = 'return'

            borrow = df_new.merge(df, how='inner', on=list(df_new.columns))
            borrow['return or borrow'] = 'borrow'

            df_new = pd.concat([prev_return, borrow]).drop_duplicates(keep=False)
            print(df_new.to_string(max_colwidth=30))

            print('////////////////////////')
            # determine whether lib info changes
            if df_new.empty:
                print('no difference')

            else:
                print('return or borrow books')
                print(df_new.to_string(max_colwidth=30))

                dbx.files_upload(df.to_csv(index=False).encode('utf-8'), f"/{filename}", mode=dropbox.files.WriteMode.overwrite)
                send_mail(df_new.to_string(index=False, max_colwidth=30), subject='Library Assistant', files=["lib_assist_current.csv"])
                print("Successfully send out difference & save to dropbox!")

                # record my borrowing history
                df_record = df_prev.append(df_new)
                dbx.files_upload(df_record.to_csv(index=False).encode('utf-8'), "/lib_assist_history.csv", mode=dropbox.files.WriteMode.overwrite)

            # determine whether return day is close
            next_deadline = df['deadline'][0]
            now = datetime.datetime.now()
            deadline = datetime.datetime.strptime(next_deadline, '%d.%m.%Y')
            remaining_days = deadline - now
            print('////////////////////////')
            print(f"remaining_days: {remaining_days.days}")
            
            if remaining_days.days > 7:
                print('not urgent to return books')

            else:
                dbx.files_upload(df.to_csv(index=False).encode('utf-8'), f"/{filename}",
                                mode=dropbox.files.WriteMode.overwrite)
                df_return = df[df['deadline'] == next_deadline]
                print(df_return.to_string(max_colwidth=30))
                send_mail(df_return.to_string(max_colwidth=30, index=False), subject=f'Library Assistant: deadline {next_deadline}', files=["lib_assist_current.csv"])
                print("Successfully send out a return notification!")
    except Exception as e:
        print(e)
        print('no book borrowing')
